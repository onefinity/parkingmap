(function() {
    var app = angular.module('parkingMap', ['ui.router', 'LocalStorageModule', 'ngMap']);

    app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        // Enable FastClick
        FastClick.attach(document.body);

        // Routes
        $stateProvider
            .state('map', {
                url: '/',
            })
            .state('saved-locations', {
                url: '/saved-locations',
                templateUrl: 'templates/saved-locations.html',
            })
            .state('saved-locations.add', {
                url: '/add',
            })
            .state('current-location', {
                url: '/current-location',
                templateUrl: 'templates/current-location.html',
            })
            .state('preferences', {
                url: '/preferences',
                templateUrl: 'templates/preferences.html',
            })

        // Routing
        $urlRouterProvider.otherwise('/');
    }]);

    app.controller('savedLocationsController', ['$scope', '$state', 'SavedLocations', function($scope, $state, SavedLocations) {
        $scope.locations = SavedLocations.locations;
        $scope.showAddLocation = false;

        $scope.addLocation = function() {
            var location = {
                title: $scope.addTitle,
                showOnMap: $scope.addShowOnMap,
                latitude: $scope.location.latitude,
                longitude: $scope.location.longitude,
                timestamp: new Date(),
            }

            // Add location
            SavedLocations.addLocation(location);

            // Clear and hide form
            $scope.resetForm();
            $state.go('saved-locations');
        }
        $scope.removeLocation = function(index) {
            if ( confirm('Are you sure you want to delete this location?') ) {
                SavedLocations.removeLocation(index);
            }
        }
        $scope.showForm = function() { $scope.showAddLocation = true; }
        $scope.resetForm = function() {
            $scope.showAddLocation = false; 

            $scope.addTitle = '';
            $scope.addShowOnMap = true;
        }
    }]);
    app.controller('mapController', ['$scope', 'SavedLocations', function($scope, SavedLocations) {
        $scope.locations = SavedLocations.locations;
        $scope.settings = {
            latitude: 52.2129918,
            longitude: 5.2793703,
            zoom: 8,
        }
    }]);
    app.controller('preferencesController', ['$scope', '$state', 'SavedLocations', 'localStorageService', function($scope, $state, SavedLocations, localStorageService) {
        $scope.preferences = {}
        $scope.$state = $state;
        $scope.locations = SavedLocations.locations;
        $scope.location = {
            latitude: null,
            longitude: null,
        }

        $scope.getLocation = function() {
            if ( !'geolocation' in navigator ) {
                alert('Geolocation is not supported in this browser.')
                return false;
            }

            navigator.geolocation.getCurrentPosition(function(position) {
                $scope.$apply(function() {
                    $scope.location.latitude = position.coords.latitude.toFixed(5);
                    $scope.location.longitude = position.coords.longitude.toFixed(5);
                });
            });
        }();

        // ID
        $scope.updateID = function(id) {
            if ( typeof id === 'undefined' ) {
                var saved_id = localStorageService.get('id');
                id = ( saved_id ) ? saved_id : (Math.random() + 1).toString(36).slice(-8);
            }

            localStorageService.set('id', id)
            $scope.preferences.id = id;
        };
        $scope.updateID();
    }]);

    // Factories
    app.factory('SavedLocations', ['localStorageService', function(localStorageService) {
        return {
            locations: localStorageService.get('locations') || [],

            addLocation: function(location) {
                this.locations.push(location);
                this.save();
            },
            removeLocation: function(index) {
                this.locations.splice(index, 1);
                this.save();
            },

            save: function() {
                localStorageService.set('locations', this.locations);
            }
        }
    }]);
})(jQuery);